//importing function
const counterFactory = require('../counterFactory.cjs');
//invoking counterFactory functions
const result = counterFactory();
//logging result increment key value
console.log(result.increment());
//logging result decrement key value
console.log(result.decrement());
