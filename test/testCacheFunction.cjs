// importing function
const cacheFunction = require("../cacheFunction.cjs");
//function declaration cache generator
function cacheGenerator(...args) {
  return JSON.stringify(args);
}
try {
  const result = cacheFunction(cacheGenerator);
  console.log(result(1, 2, 3));
} catch (error) {
  console.log(error);
}
