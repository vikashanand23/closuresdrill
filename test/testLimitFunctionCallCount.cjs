//importing function
const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");
//callback function
const limitCall = function (call) {
  return call;
};

try {
  const result = limitFunctionCallCount(limitCall, 3);
  console.log(result());
} catch (error) {
  console.log(error);
}
