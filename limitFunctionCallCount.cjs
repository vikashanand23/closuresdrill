function limitFunctionalCount(cb, n) {
  let call = 0;
  if (typeof cb !== "function") {
    throw new Error("Expecting first parameter as function");
  }
  if (typeof n === "number" && n > 0) {
    return function (...args) {
      if (call < n) {
        call++;
        return cb.apply(null, arguments);
      } else {
        return null;
      }
    };
  } else {
    throw new Error("n must be a positive number.");
  }
}

module.exports = limitFunctionalCount;
